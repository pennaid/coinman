module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "object-curly-newline": ["error", {
      "ObjectPattern": { "multiline": false },
    }],
    "consistent-return": 0,
  }
};
