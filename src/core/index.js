const binance = require('./binance');
const telegram = require('./telegram');

module.exports = {
  binance,
  telegram,
};
